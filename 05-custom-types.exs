# https://stackoverflow.com/questions/39576209/elixir-cannot-access-struct

defmodule User do
  defstruct name: "John", age: 27
end

defmodule Human do
  defstruct [:name, :age]
end

defprotocol Greetings do
  def hello(human, message)
  def hey(human, message)
end

# It's ike a capability
defimpl Greetings, for: Human do
  def hello(%Human{name: name, age: age}, message) do
    IO.puts("👋 Hello I'm #{name}, #{message}")
  end
  
  def hey(%Human{name: name, age: age}, message) do
    IO.puts("😃 Hey #{name}, #{message}")
  end
end


defmodule Main do
  def start do
    john = %User{}
    IO.puts(john.name)
    IO.puts(john.age)
    
    bob = %Human{name: "Bob", age: 42}
    IO.puts("#{bob.name} #{bob.age}")
    
    sam = %Human{name: "Sam", age: 42}
    sam
    |> Greetings.hello("hey")
    
    bob
    |> Greetings.hello("coucou")
    
    bob
    |> Greetings.hey("coucou")
    |> IO.inspect() # c'est quoi ? Piping
    # https://github.com/savoisn/elixir-workshop/blob/master/documentation/Language_Intro.adoc
    
    # et ça |> c'est quoi ?
    
  end
end

Main.start()