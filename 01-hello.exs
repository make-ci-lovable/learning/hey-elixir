name = "Bob Morane"
IO.puts(name)
IO.puts("Hello #{name}")

say_hello = fn first, last ->
  IO.puts("👋 Hello, I'm #{first} #{last}")
end

# 🖐️ don't forget the dot after the function name!
say_hello.("Bob","Morane")
