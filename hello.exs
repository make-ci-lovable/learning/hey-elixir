IO.puts "Hello 🖖 from Elixir"
add = fn a, b -> a + b end
File.write("add.txt", "🤖 > 40+2 = #{add.(40,2)}")

case File.read("add.txt") do
  {:ok, body}      -> IO.puts body
  {:error, reason} -> IO.puts reason
end
