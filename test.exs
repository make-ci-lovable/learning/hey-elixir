defmodule Circle do
  def pi, do: 3.14159
  def area(r), do:
    pi() * (r ^ 2)
  def circumference(r), do:
    2 * pi() * r
end

Circle.area(2) # 4π